create database contracts;

CREATE TABLE `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sysytem` varchar(45) NOT NULL,
  `order_number` varchar(45) NOT NULL,
  `from_date` DATE NOT NULL,
  `to_date` DATE NOT NULL,
  `amount` integer NOT NULL,
  `amount_type` varchar(45) NOT NULL,
  `amount_period` varchar(45) NOT NULL,
  `authorization_percent` varchar(45) NOT NULL,
  `active` boolean NOT NULL,
  `request` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

