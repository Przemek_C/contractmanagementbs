/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pc.controller;

import com.pc.model.Contract;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.pc.dao.ContractDAO;
import java.util.logging.Logger;

/**
 *
 * @author Przemek
 */
@Controller
public class ContractController {

    private static Logger logger = Logger.getLogger(ContractController.class.getName());

    @Autowired
    private ContractDAO contractDao;

    @RequestMapping(value = "/contracts", method = RequestMethod.GET)
    public String listContract(Model model) {
        model.addAttribute("contract", new Contract());
        model.addAttribute("contractList", contractDao.contractList());
        logger.info("listContract");
        return "contract";
    }

    @RequestMapping(value = "/contract/add", method = RequestMethod.POST)
    public String addContract(@ModelAttribute("contract") Contract contract) {
        logger.info("addContract");

        if (contract.getId() == null || contract.getId() == 0) {

            contractDao.saveOrUpdateContract(contract);
        }

        return "redirect:/contracts";
    }

    @RequestMapping("/contract/remove/{id}")
    public String removeContract(@PathVariable("id") int id) {
        logger.info("removeContract");

        contractDao.deleteContract(id);
        return "redirect:/contracts";
    }

    @RequestMapping("/contract/edit/{id}")
    public String editContract(@PathVariable("id") int id, Model model) {
        logger.info("editContract");
        model.addAttribute("contract", contractDao.getContractById(id));
        model.addAttribute("contractList", contractDao.contractList());
        return "contract";
    }
}
