<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!--
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <p>Hello! This is the default welcome page for a Spring Web MVC project.</p>
        <p><i>To display a different welcome page for this project, modify</i>
            <tt>index.jsp</tt> <i>, or create your own welcome page then change
                the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
                welcome page and also update the welcome-file setting in</i>
            <tt>web.xml</tt>.</p>
    </body>
</html>-->

<%-- 
    Document   : contracts
    Created on : 2018-04-25, 17:05:53
    Author     : Przemek
--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="for" %>
<%@ page session="false" %>
<html

    xmlns:h="http://www.sun.com/jsf/html">
    <head>
        <title>Contract Management</title>
        <style type="text/css">
            .empTable  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
            .empTable td{font-family:Arial, sans-serif;font-size:16px;padding:10px 5px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;border-color:#ccc;color:#00FF00;background-color:#FF0000;}
            .empTable th{font-family:Arial, sans-serif;font-size:16px;font-weight:normal;padding:10px5px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;border-color:#ccc;color:#000000;background-color:#FF4500;}
            .empTable .empTable-4eph{background-color:#C0C0C0}
        </style>

        <script>
            $(document).ready(function () {
                $('#contractTable').DataTable();
            });
        </script>
    </head>
    <body>

        <h1>Contract Management</h1>
        <p style="border-bottom:1px solid black;"></p>

        <c:url var="addAction" value="/contract/add" ></c:url>

        <form:for action="${addAction}" modelAttribute="contract">
        <table>
            <c:if test="${not empty contract.id}"/>
            <h1><message:spring code="EditContract"/></h1>
            <tr>
                <td>
            <form:label path="id">
                <message:spring code="id"/>
            </form:label>
            </td>
            <td>
            <form:input path="id" readonly="true" size="8"  disabled="true" />
            <form:hidden path="id" />
            </td>
            </tr>


            <tr>
                <td>
            <form:label path="system">
                <message:spring code="System"/>
            </form:label>
            </td>
            <td>
            <form:input path="system" />
            </td>
            </tr>

            <tr>
                <td>
            <form:label path="lastName">
                <message:spring code="LastName"/>
            </form:label>
            </td>
            <td>
            <form:input path="lastName" />
            </td>
            </tr>

            <tr>
                <td>
            <form:label path="age">
                <message:spring code="Age"/>
            </form:label>
            </td>
            <td>
            <form:input path="age" />
            </td>
            </tr>

            <tr>
                <td>
            <form:label path="education">
                <message:spring code="Education"/>
            </form:label>
            </td>
            <td>
            <form:input path="education" />
            </td>
            </tr>

            <tr>
                <td>
            <form:label path="salary">
                <message:spring code="Salary"/>
            </form:label>
            </td>
            <td>
            <form:input path="salary" />
            </td>
            </tr>

            <tr>
                <td colspan="2">
            <input type="submit" <message:spring code="Submit"/>
            </td>
            </tr>

        </table>   
    </form:form>
    <br>

    <c:if test="${not empty contractList}"/>
    <h3><message:spring code="contractList"/></h3>
    <table id="contractTable" class="empTable">
        <thead>
            <tr>
                <th width="50">ID</th>
                <th width="90">System</th>
                <th width="90">Order Number</th>
                <th width="90">From Date</th>
                <th width="90">To Date</th>
                <th width="90">Amount</th>
                <th width="90">Amount Number</th>
                <th width="90">Amount Date</th>
                <th width="90">Authorization Percent</th>
                <th width="90">Active</th>
                <th width="90">Request</th>
                <th width="60">Edit</th>
                <th width="60">Delete</th>
            </tr>
        </thead>
        <c:forEach items="${contractList}" var="contract">
            <tr>
                <td>${contract.id}</td>
                <td>${contract.system}</td>
                <td>${contract.order_number}</td>
                <td>${contract.from_date}</td>
                <td>${contract.to_date}</td>
                <td>${contract.amount}</td>
                <td>${contract.amount_type}</td>
                <td>${contract.amount_period}</td>
                <td>${contract.authorization_percent}</td>
                <td>${contract.active}</td>
                <td>${contract.request}</td>
                <td><a href="<c:url value='/contract/edit/${contract.id}' />" >Edit</a></td>
                <td><a href="<c:url value='/contract/remove/${contract.id}' />" >Delete</a></td>
            </tr>
        </c:forEach>
    </table>
    <br>

    </body>

    </html>