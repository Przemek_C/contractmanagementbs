/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pc.dao;

import com.pc.model.Contract;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Przemek
 */
@Repository
public class ContractDAOImpl implements ContractDAO {

    private static Logger logger = Logger.getLogger(ContractDAOImpl.class.getName());

    @Autowired
    private SessionFactory sessionFactory;

    public ContractDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public ContractDAOImpl() {
    }

    @Override
    @Transactional
    public void saveOrUpdateContract(Contract c) {
        logger.log(Level.INFO, "saveOrUpdateContract ", c.getId());
        this.sessionFactory.getCurrentSession().saveOrUpdate(c);
        logger.log(Level.INFO, "saveOrUpdateContract sessonFactory", this.sessionFactory.getCurrentSession());
    }

    @Override
    @Transactional
    public Contract getContractById(int id) {
        String select = "from contract where id=" + id;
        Query query = sessionFactory.getCurrentSession().createQuery(select);
        logger.log(Level.INFO, "getContractById - query: " + query.toString());

        @SuppressWarnings("unchecked")
        List<Contract> contractList = (List<Contract>) query.list();

        if (contractList != null && !contractList.isEmpty()) {
            logger.log(Level.INFO, "getContractById: {0}", contractList.get(0));
            return contractList.get(0);
        }
        return null;
    }

    @Override
    @Transactional
    public void deleteContract(int id) {
        logger.log(Level.INFO, "deleteContract ", id);

        Contract contract = new Contract();
        contract.setId(id);
        sessionFactory.getCurrentSession().delete(contract);
    }

    @Override
    @Transactional
    public List<Contract> contractList() {
        @SuppressWarnings("unchecked")
        List<Contract> contractList = (List<Contract>) sessionFactory.getCurrentSession()
                .createCriteria(Contract.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        for (Contract c : contractList) {
            logger.log(Level.INFO, "contractList ", c + "/n");
        }
        return contractList;
    }

}
